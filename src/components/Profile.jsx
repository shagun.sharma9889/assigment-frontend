import {
  Container,
  Box,
  FormControl,
  FormLabel,
  FormControlLabel,
  Typography,
} from "@mui/material";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Modal from "@mui/material/Modal";
import React, { useEffect, useState } from "react";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import axios from "axios";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const centeredContainerStyle = {
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  height: "50vh",
};
const Profile = () => {
  const [profileData, setProfileData] = useState("");
  const [allProfileData, setAllProfileData] = useState([]);
  const [open, setOpen] = React.useState(false);
  let token = localStorage.getItem("token");
  const getUserProfile = () => {
    axios
      .get("http://localhost:8000/api/auth/user-profile", {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      })
      .then((data) => {
        setProfileData(data.data.data);
      });

    axios
      .get("http://localhost:8000/api/auth/get-all-profiles", {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      })
      .then((data) => {
        setAllProfileData(data.data.data);
      });
  };

  useEffect(() => {
    getUserProfile();
  }, []);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    let newData = {
      first_name: data.get("first_name"),
      last_name: data.get("last_name"),
      email: data.get("email"),
      dob: data.get("dob"),
      gender: "male",
    };

    axios
      .post("http://localhost:8000/api/auth/profile-create", newData, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      })
      .then((data) => {
        console.log(data);
      });
  };

  return (
    <div>
      <Button onClick={handleOpen}>Create Profile</Button>
      <Container>
        <Typography variant="h4" component="h1">
          John Doe
        </Typography>
        <Typography variant="subtitle1" component="p">
          Email: johndoe@example.com
        </Typography>
        <Typography variant="subtitle1" component="p">
          Gender: Male
        </Typography>
        <Typography variant="subtitle1" component="p">
          Date of Birth: January 1, 1990
        </Typography>
      </Container>
      <div>
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style} component="form" onSubmit={handleSubmit} noValidate>
            <TextField
              margin="normal"
              required
              fullWidth
              id="first_name"
              label="First Name"
              name="first_name"
              autoComplete="First Name"
              autoFocus
            />
            <TextField
              margin="normal"
              required
              fullWidth
              id="last_name"
              label="Last Name"
              name="last_name"
              autoComplete="last_name"
              autoFocus
            />
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              id="date"
              label="Date of Birth"
              type="date"
              name="dob"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <div>
              <FormControl>
                <FormLabel id="demo-controlled-radio-buttons-group">
                  Gender
                </FormLabel>
                <RadioGroup
                  aria-labelledby="demo-controlled-radio-buttons-group"
                  name="controlled-radio-buttons-group"
                >
                  <FormControlLabel
                    value="female"
                    name="female"
                    control={<Radio />}
                    label="Female"
                  />
                  <FormControlLabel
                    value="male"
                    name="email"
                    control={<Radio />}
                    label="Male"
                  />
                </RadioGroup>
              </FormControl>
            </div>
            <Button variant="contained" type="submit">
              Submit
            </Button>
          </Box>
        </Modal>
        <div>
          <table>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Email</th>
              <th>Gender</th>
            </tr>

            {allProfileData.map((item, index) => {
              return (
                <>
                  <tr>
                    <td>{item.id}</td>
                    <td>{item.first_name + item.last_name}</td>
                    <td>{item.email}</td>
                    <td>{item.gender}</td>
                  </tr>
                </>
              );
            })}
          </table>
        </div>
      </div>
    </div>
  );
};

export default Profile;
