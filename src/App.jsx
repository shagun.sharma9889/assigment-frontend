import { useState } from "react";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import Navbar from "./components/Navbar";
import Home from "./components/Home";
import Profile from "./components/Profile";
import Reports from "./components/Reports";
import LogIn from "./components/Login";
import Register from "./components/Register";
import Protected from "./ProtectedRoutes/Protected";

function App() {
  let token = localStorage.getItem("token");
  return (
    <>
      <Router>
        <div>
          {token && <Navbar />}
          <Routes>
            <Route exact path="/" element={<Protected />}>
              <Route exact path="/" element={<Home />}></Route>
              <Route exact path="/reports" element={<Reports />}></Route>
              <Route exact path="/profile" element={<Profile />}></Route>
            </Route>
            <Route exact path="/login" element={<LogIn />}></Route>
            <Route exact path="/register" element={<Register />}></Route>
          </Routes>
        </div>
      </Router>
    </>
  );
}

export default App;
