import React from "react";
import { Outlet, Navigate } from "react-router-dom";
import LogIn from "../components/Login";

const Protected = () => {
  let token = localStorage.getItem("token");
  return <div>{token ? <Outlet /> : <Navigate to="/login" />}</div>;
};

export default Protected;
